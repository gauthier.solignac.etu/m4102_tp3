| URI                      | Opération   | MIME                                                         | Requête      | Réponse                                                          |
| :----------------------- | :---------- | :----------------------------------------------------------- | :----------- | :------------------------------------------------------------    |
| /commandes               | GET         | <-application/json<br><-application/xml                      |              | liste des commandes (C3)                                         |
| /commandes  /{id}        | GET         | <-application/json<br><-application/xml                      |              | une commande (C3) ou 404                                         |
| /commandes  /{id}/name   | GET         | <-text/plain                                                 |              | le nom de la commande ou 404                                     |
| /commandes               | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Commande (C2)| Nouvelle commande (C2)<br>409 si la pizza existe déjà (même nom) |
| /commandes  /{id}        | DELETE      |                                                              |              |                                                                  |

Une commande comporte uniquement un identifiant et un nom. Sa
représentation JSON (C3) prendra donc la forme suivante :
~~~json
{
	"id": "dcc2ac08-b735-4db3-bb7f-02265af334d0",
	"name": "Gotié Solignac",
	"pizzas" :
	[
		{
			"id": "0474e9f7-c634-4b6c-bbf1-0be862327a70",
			"name": "Mozza",
			"ingredients" :
			[
				{
		  			"id": "3fcc9166-7033-45b8-b3a4-0a075564ede8",
					"name": "champignons de paris"
				},
				{
		  			"id": "310f090f-65d5-4db9-a038-df5045734f66",
					"name": "jambon blanc"
				},
				{
		  			"id": "7f9bf936-076f-4248-a8d6-528cb51f8849",
					"name": "gruyère rapé"
				}
			]
		},
		{
			"id": "0fcffe07-c11d-4592-8f5d-fe636479287d",
			"name": "Cannibale",
			"ingredients" :
			[
				{
		  			"id": " 7348897f-e8b4-4852-94ce-08948eade68d ",
					"name": "Chorizo"
				},
				{
		  			"id": "a6775f40-cecd-4b9d-98c3-9daffc49df80",
					"name": "Merguez"
				},
				{
		  			"id": "7f9bf936-076f-4248-a8d6-528cb51f8849",
					"name": "gruyère rapé"
				}
			]
		}
	]
}
~~~
Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente une pizza. Aussi on aura une
représentation JSON (C2) qui comporte uniquement le nom et les id des pizzas :
~~~json
{
	"name": "Patrick Chirac",
	"pizzas" :
	[
		{
			"id": "0474e9f7-c634-4b6c-bbf1-0be862327a70"
		},
		{
			"id": "0fcffe07-c11d-4592-8f5d-fe636479287d"
		}
	]
}
~~~