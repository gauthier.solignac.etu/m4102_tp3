| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /pizza             | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas (P2)                                           |
| /pizza/{id}        | GET         | <-application/json<br><-application/xml                      |                 | une pizza (P1) ou 404                                            |
| /pizza/{id}/name   | GET         | <-text/plain                                                 |                 | le nom de la pizza ou 404                                        |
| /pizza             | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza (P1)      | Nouvelle pizza (P1)<br>409 si les ingredients exisitent |
| /pizza/{id}        | DELETE      |                                                              |                 |                                                                      |

Une pizza comporte uniquement un identifiant et un nom et des ingredients. Sa
représentation JSON (P2) prendra donc la forme suivante :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "MozzaThéon",
      "ingredients" : ["mozzarella", "theon"]
    }

Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente un ingrédient. Aussi on aura une
représentation JSON (P1) qui comporte uniquement le nom :

    { 
    	"name": "MozzaThéon", "ingredients" : 
    	["mozzarella", "theon"]
    }
    