package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class CommandeDto {
    private UUID id;
    private String name;
    private List<Pizza> pizzas;

    public CommandeDto() {
    	this.pizzas = new ArrayList<>();
    }

    public void setId(UUID id) {
        this.id = id;
        
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

	public List<Pizza> getIngredients() {
		return pizzas;
	}

	public void setIngredients(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
}