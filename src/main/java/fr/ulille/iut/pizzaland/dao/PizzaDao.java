package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (idPizza VARCHAR(128), idIngredient VARCHAR(128), PRIMARY KEY(idPizza, idIngredient), "
			+ "FOREIGN KEY(idPizza) REFERENCES Pizzas(id) ON DELETE CASCADE, FOREIGN KEY(idIngredient) REFERENCES ingredients(id))")
	void createAssociationTable();
	
	@SqlUpdate("DROP TABLE IF EXISTS Pizzas")
    void dropPizzaTable();
	
	@SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
    void dropAssociationTable();
	
	@SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name)")
    void insert(@BindBean Pizza pizza);
	
	@SqlUpdate("INSERT INTO PizzaIngredientsAssociation (idPizza, idIngredient) VALUES (:pizza.id, :ingredient.id)")
	void insert(@BindBean("pizza") Pizza pizza, @BindBean("ingredient") Ingredient ingredient);
	
	@SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
    void remove(@Bind("id") UUID id);

	@Transaction
	default void createTableAndIngredientAssociation() {
		createAssociationTable();
		createPizzaTable();
	}
	
	@Transaction
	default void dropTableAndIngredientAssociation() {
		dropAssociationTable();
		dropPizzaTable();
	}
	
	@Transaction
	default void insertWithIngredients(Pizza pizza) {	
		insert(pizza);
		
		for(Ingredient i: pizza.getIngredients()) {
			insert(pizza, i);
		}
	}
	
	@SqlQuery("SELECT * FROM Pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();

	@SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(@Bind("id") UUID id); 
	
	@SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);
}