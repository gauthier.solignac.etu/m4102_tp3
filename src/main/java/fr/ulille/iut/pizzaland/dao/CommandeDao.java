package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

public interface CommandeDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Commandes (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createCommandeTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS CommandePizzasAssociation (idCommande VARCHAR(128), idPizzas VARCHAR(128), PRIMARY KEY(idCommande, idPizzas), "
			+ "FOREIGN KEY(idCommande) REFERENCES Commandes(id) ON DELETE CASCADE, FOREIGN KEY(idPizzas) REFERENCES pizzas(id))")
	void createAssociationTable();
	
	@SqlUpdate("DROP TABLE IF EXISTS Commandes")
    void dropCommandeTable();
	
	@SqlUpdate("DROP TABLE IF EXISTS CommandePizzasAssociation")
    void dropAssociationTable();
	
	@SqlUpdate("INSERT INTO Commandes (id, name) VALUES (:id, :name)")
    void insert(@BindBean Commande commande);
	
	@SqlUpdate("INSERT INTO CommandePizzasAssociation (idCommande, idPizzas) VALUES (:commande.id, :pizzas.id)")
	void insert(@BindBean("commande") Commande commande, @BindBean("pizzas") Pizza pizzas);
	
	@SqlUpdate("DELETE FROM Commandes WHERE id = :id")
    void remove(@Bind("id") UUID id);

	@Transaction
	default void createTableAndCommandeAssociation() {
		createAssociationTable();
		createCommandeTable();
	}
	
	@Transaction
	default void dropTableAndCommandeAssociation() {
		dropAssociationTable();
		dropCommandeTable();
	}
	
	@Transaction
	default void insertWithPizzas(Commande commande) {
		insert(commande);
		for(Pizza p: commande.getPizzas()) {
			insert(commande, p);
		}
	}
	
	@SqlQuery("SELECT * FROM Commandes")
	@RegisterBeanMapper(Commande.class)
	List<Commande> getAll();

	@SqlQuery("SELECT * FROM Commandes WHERE id = :id")
	@RegisterBeanMapper(Commande.class)
	Commande findById(@Bind("id") UUID id); 
	
	@SqlQuery("SELECT * FROM Commandes WHERE name = :name")
    @RegisterBeanMapper(Commande.class)
    Commande findByName(@Bind("name") String name);
}