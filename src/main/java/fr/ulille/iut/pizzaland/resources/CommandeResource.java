package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Path("/commandes")
public class CommandeResource {
    private static final Logger LOGGER = Logger.getLogger(CommandeResource.class.getName());
    private CommandeDao commandes;
    
    @Context
    public UriInfo uriInfo;
    
    public CommandeResource() {
        commandes = BDDFactory.buildDao(CommandeDao.class);
        commandes.createCommandeTable();
        commandes.createAssociationTable();
    }
    
    @POST
    public Response createCommande(CommandeCreateDto CommandeCreateDto) {
        Commande existing = commandes.findByName(CommandeCreateDto.getName());
        if (existing != null) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        try {
            Commande commande = Commande.fromCommandeCreateDto(CommandeCreateDto);
            commandes.insertWithPizzas(commande);
            CommandeDto commandeDto = Commande.toDto(commande);
            
            URI uri = uriInfo.getAbsolutePathBuilder().path(commande.getId().toString()).build();

            return Response.created(uri).entity(commandeDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }
    
    @DELETE
    @Path("{id}")
    public Response deleteCommande(@PathParam("id") UUID id) {
      if ( commandes.findById(id) == null ) {
        throw new WebApplicationException(Response.Status.NOT_FOUND);
      }

      commandes.remove(id);

      return Response.status(Response.Status.ACCEPTED).build();
    }
    
    @GET
    @Path("{id}/name")
    public String getPizzaName(@PathParam("id") UUID id) {
        Commande commande = commandes.findById(id);

        if (commande == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        return commande.getName();
    }
    
    @GET
    public List<CommandeDto> getAll(){
    	LOGGER.info("CommandeRessource:getAll");
    	
    	List<CommandeDto> l = commandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
    	LOGGER.info(l.toString());
    	return l;
    }
    
    @GET
    @Path("{id}")
    @Produces({ "application/json", "application/xml" })
    public CommandeDto getOneCommande(@PathParam("id") UUID id) {
        LOGGER.info("getOneCommande(" + id + ")");
        try {
            Commande commande = commandes.findById(id);
            LOGGER.info(commande.toString());
            return Commande.toDto(commande);
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }
    
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response createCommande(@FormParam("name") String name) {
      Commande existing = commandes.findByName(name);
      if (existing != null) {
        throw new WebApplicationException(Response.Status.CONFLICT);
      }

      try {
        Commande commande = new Commande();
        commande.setName(name);
        
        List<Pizza> commandesList = new ArrayList<>();
        commandesList.add(new Pizza("Cannibale"));
        commande.setPizzas(commandesList);
        
        commandes.insert(commande);

        CommandeDto commandeDto = Commande.toDto(commande);

        URI uri = uriInfo.getAbsolutePathBuilder().path("" + commande.getId()).build();

        return Response.created(uri).entity(commandeDto).build();
      } catch (Exception e) {
          e.printStackTrace();
          throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
      }
    }

}