package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class CommandeRessourceTest extends JerseyTest{
	private CommandeDao dao;
	private static final Logger LOGGER = Logger.getLogger(CommandeRessourceTest.class.getName());

	@Override
	protected Application configure() {
		return new ApiV1();
	}
	
	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(CommandeDao.class);
		//dao.createPizzaTable();
		dao.createTableAndCommandeAssociation();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTableAndCommandeAssociation();
	}
	
	@Test
	public void testGetEmptyList() {

		Response response = target("/commandes").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		List<CommandeDto> commandes;
		commandes = response.readEntity(new GenericType<List<CommandeDto>>() {
		});

		assertEquals(0, commandes.size());
	}

	@Test
	public void testGetExistingCommande() {
		Commande commande = new Commande();
		commande.setName("Pierick");
		dao.insertWithPizzas(commande);

		Response response = target("/commandes").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
		assertEquals(commande, result);
	}

	@Test
	public void testGetNotExistingPizza() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}
	
	@Test
	public void testCreateCommande() {
		Commande commande = new Commande();
		Pizza pizza = new Pizza();
		Ingredient ingredient1 = new Ingredient();
		Ingredient ingredient2 = new Ingredient();

		
		ingredient1.setName("Mozarela");
		ingredient2.setName("Ketchup");
		List<Ingredient> ingredients = new ArrayList<>();
		ingredients.add(ingredient1);
		ingredients.add(ingredient2);
		
		pizza.setName("Chorizo");
		pizza.setIngredients(ingredients);
		
		List<Pizza> pizzas = new ArrayList<>();
		pizzas.add(pizza);
		
		commande.setName("Cannibale");
		commande.setPizzas(pizzas);
		
		System.out.println("PIZZA TEST =====================");
		System.out.println(commande);
		
		PizzaDto pizzaDto = Pizza.toDto(pizza);
        target("/pizzas").request().post(Entity.json(pizzaDto));
        
		CommandeCreateDto commandeCreateDto = Commande.toCreateDto(commande);
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

		assertEquals(target("/commandes/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), commandeCreateDto.getName());
	}
	
	@Test
	public void testCreateSameCommande() {
		Commande commande = new Commande("Pierick");
		dao.insertWithPizzas(commande);

		CommandeCreateDto commandeCreateDto = Commande.toCreateDto(commande);
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testCreatePizzaWithoutName() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

		Response response = target("/commandes").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testDeleteExistingCommande() {
		Commande commande = new Commande();
		commande.setName("Cannibale");
		dao.insertWithPizzas(commande);

		Response response = target("/commandes/").path(commande.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());
		
		Commande result = dao.findById(commande.getId());
		assertEquals(result, null);
	}
	
	@Test
	public void testDeleteNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test
	   public void testGetCommandeName() {
	     Commande commande = new Commande();
	     commande.setName("Cannibale");
	     dao.insertWithPizzas(commande);

	     Response response = target("commandes").path(commande.getId().toString()).path("name").request().get();

	     assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	     assertEquals("Cannibale", response.readEntity(String.class));
	  }
	
	@Test
	  public void testGetNotExistingCommandeName() {
	    Response response = target("commandes").path(UUID.randomUUID().toString()).path("name").request().get();

	    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	  }
	
	@Test
    public void testCreateWithForm() {
        Form form = new Form();
        form.param("name", "cannibale");

        Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        Response response = target("commandes").request().post(formEntity);

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        String location = response.getHeaderString("Location");
        String id = location.substring(location.lastIndexOf('/') + 1);
        Commande result = dao.findById(UUID.fromString(id));

        assertNotNull(result);
    }
}
